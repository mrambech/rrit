<!DOCTYPE html>
<html>
		<head>
			<meta charset="UTF-8"/>
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<link type="text/css" rel="stylesheet" href="stylesheet.css"/>
			<script src="myscript.js"></script>
			<link rel="icon" href="bilder/icon.jpg">
			<title>Registrering</title>
		</head>
	<body>

		<!-- Navigasjonsmeny -->
		<div class="menu_bar"></div>
		<div class = "menu_content">
			<div class="menu_title">
				<h1 class = "header_title_main"> RRIT </h1>
				<h2 class ="header_title_sub">Rognes & Rambech</h2>
				<div class="white_line"></div>
			</div>
			<div class = "menu_list">
				<nav>
					<p><a href= "index.php">Hjem</a></p>
					<p><a href = "about.php">Om oss</a></p>
					<p><a href = "contact.php">Kontakt</a></p>
					<p><a href="CV.php">Tidligere Arbeid</a></p>
					<p><a href="login.php">Logg inn her!</a></p>
				</nav>
			</div>
		</div>
		
		<!-- Kropp -->
		<div class="background_image"></div>
		<div class = "body">
		<h1 class= "page_headline">Registrering</h1>
		<div class="white_line_headline"></div>

		<div class = "login_form">
		<form action="register_process.php" method="POST">
			<input type="text" name="first" placeholder="Fornavn"><br>
			<input type="text" name="last" placeholder="Etternavn"><br>
			<input type="text" name="uid" placeholder="Brukernavn"><br>
			<input type="password" name="pwd" placeholder="Passord"><br>
			<button type="submit">Registrer</button>
		</form>
		</div>








		</div>
	</body>
</html>