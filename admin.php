<!DOCTYPE html>
<html>
		<head>
			<meta charset="UTF-8"/>
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<link type="text/css" rel="stylesheet" href="stylesheet.css"/>
			<script src="myscript.js"></script>
			<link rel="icon" href="bilder/icon.jpg">
			<title>Om Oss</title>
		</head>
	<body>
		<!-- Navigasjonsmeny -->
		<div class="menu_bar"></div>
		<div class = "menu_content">
			<div class="menu_title">
				<h1 class = "header_title_main"> RRIT </h1>
				<h2 class ="header_title_sub">Rognes & Rambech</h2>
				<div class="white_line"></div>
			</div>
			<div class = "menu_list">
				<nav>
					<p><a href= "index.php">Hjem</a></p>
					<p><a href = "about.php">Om oss</a></p>
					<p><a href = "contact.php">Kontakt</a></p>
					<p><a href="CV.php">Tidligere Arbeid</a></p>
					<p><a href="register.php">Registrer</a></p>
				</nav>
			</div>
		</div>
		
		<!-- Kropp -->
		<div class="background_image"></div>
		<div class = "body">
			<h1 class= "page_headline"> Kontrollpanel </h1>
			<div class="white_line_headline"></div>
		
			<button id = "load_comments" onclick="showComments()">Vis / Skjul Meldinger</button>

			<div id = "comments">
				<h2>Meldinger:</h2>
				<div class = "white_line_comments"></div>
					<?php 
						//Koble til databasen
						$servername = "mysql04.uniweb.no";
						$dbusername = "d28441";
						$dbpassword = "Uphil215";
						$dbname = "d28441";
						
						// Create connection
						$conn = mysqli_connect($servername, $dbusername, $dbpassword, $dbname);
						// Check connection
						if ($conn->connect_error) {
							die("Connection failed: " .$conn->connect_error);
				
						} 

						$sql = "SELECT id,name,email,number,text FROM comments";
						$result = $conn->query($sql);

						if ($result->num_rows > 0) {
						    // output data of each row
						    while($row = $result->fetch_assoc()) {	
						    	echo "<div id ='". $row['id'] ."'>";				     
						        echo "<strong>ID:</strong><br>  " . $row["id"]. "<br>";
						        echo "<strong>Navn:</strong><br>  " . $row["name"]. "<br>";
						        echo "<strong>Email:</strong><br> " . $row["email"]. "<br>";
						        echo "<strong>Nummer:</strong><br> " . $row["number"]. "<br>";
						        echo "<strong>Melding:</strong><br> " . $row["text"]. "<br><br><br>";
						        echo "</div>";
					    	}
					    		echo '<div class = "white_line_comments"></div>';
						} 
						else {
					    echo "0 results";
						}
						$conn->close();
					?>
			</div>

		
		</div>
	</body>
</html>