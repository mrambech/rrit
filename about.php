<!DOCTYPE html>
<html>
		<head>
			<meta charset="UTF-8"/>
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<link type="text/css" rel="stylesheet" href="stylesheet.css"/>
			<script src="myscript.js"></script>
			<link rel="icon" href="bilder/icon.jpg">
			<title>Om Oss</title>
		</head>
	<body>
		<!-- Navigasjonsmeny -->
		<div class="menu_bar"></div>
		<div class = "menu_content">
			<div class="menu_title">
				<h1 class = "header_title_main"> RRIT </h1>
				<h2 class ="header_title_sub">Rognes & Rambech</h2>
				<div class="white_line"></div>
			</div>
			<div class = "menu_list">
				<nav>
					<p class = "menu_text"><a href= "index.php">Hjem</a></p>
					<p class = "menu_text"><a href = "about.php">Om oss</a></p>
					<p class = "menu_text"><a href = "contact.php">Kontakt</a></p>
					<p class = "menu_text"><a href="CV.php">Tidligere Arbeid</a></p>
					<p class = "menu_text"><a href="login.php">Kontrollpanel</a></p>
				</nav>
				<div class="white_line"></div>
			</div>
		</div>
		
		<!-- Kropp -->
		<div class="background_image"></div>
		<div class = "body">
		<h1 class= "page_headline"> Om Oss </h1>
		<div class="white_line_headline"></div>

			<div class="about_us">
				 <div class="profilpic">
		    		  <img src="bilder/icon.jpg" class="thumbnails">
		    		  <table class ="about_us_table">
		    		 	<tr>
		    		 		<th>Navn</th>
		    		 		<td>Magnus</td>
		    		 	</tr>
		    		 	<tr>
		    		 		<th>Alder</th>
		    		 		<td>21</td>
		    		 	</tr>
		    		 	<tr>
		    		 		<th>Stilling</th>
		    		 		<td>Student</td>
		    		 	</tr>
		    		 	<tr>
		    		 		<th>Studie</th>
		    		 		<td>Informasjonsvitenskap ved UiB</td>
		    		 	</tr>
		    		 </table>
		 		</div>
		 		
				 <div class="profilpic">
		    		 <img src="bilder/icon.jpg" class="thumbnails">
		    		 <table class ="about_us_table">
		    		 	<tr>
		    		 		<th>Navn</th>
		    		 		<td>Thomas</td>
		    		 	</tr>
		    		 	<tr>
		    		 		<th>Alder</th>
		    		 		<td>21</td>
		    		 	</tr>
		    		 	<tr>
		    		 		<th>Stilling</th>
		    		 		<td>Student</td>
		    		 	</tr>
		    		 	<tr>
		    		 		<th>Studie</th>
		    		 		<td>Informasjonsvitenskap ved UiB</td>
		    		 	</tr>
		    		 </table>
		    	</div>
			</div>









		</div>
	</body>
</html>