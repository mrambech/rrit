<!DOCTYPE html>
<html>
		<head>
			<meta charset="UTF-8"/>
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<link type="text/css" rel="stylesheet" href="stylesheet.css"/>
			<script src="myscript.js"></script>
			<link rel="icon" href="bilder/icon.jpg">
			<title>Om Oss</title>
		</head>
	<body>
		<!-- Navigasjonsmeny -->
		<div class="menu_bar"></div>
		<div class = "menu_content">
			<div class="menu_title">
				<h1 class = "header_title_main"> RRIT </h1>
				<h2 class ="header_title_sub">Rognes & Rambech</h2>
				<div class="white_line"></div>
			</div>
			<div class = "menu_list">
				<nav>
					<p class = "menu_text"><a href= "index.php">Hjem</a></p>
					<p class = "menu_text"><a href = "about.php">Om oss</a></p>
					<p class = "menu_text"><a href = "contact.php">Kontakt</a></p>
					<p class = "menu_text"><a href="CV.php">Tidligere Arbeid</a></p>
					<p class = "menu_text"><a href="login.php">Kontrollpanel</a></p>
				</nav>
				<div class="white_line"></div>
			</div>
		</div>
		
		<!-- Kropp -->
		<div class="background_image"></div>
		<div class = "body_contact">
		<h1 class= "page_headline"> Kontakt </h1>
		<div class="white_line_headline"></div>
		

		<!-- Kontaktskjema -->
		<div class="kontakt_skjema">
			<div class="contact_information">
			<form action="comment_process.php" method="POST">
				<label class = "label_text">Ditt navn:</label><br>
					<input type="text" name="name"> 
				<br><br>
				<label class = "label_text">Epost:</label><br>
					<input type="text" name="email"> 
				<br><br>
				<label class = "label_text">Telefon:</label><br>
					<input type="int" name="number"> 
				<br><br>
				<label class = "label_text">Din melding:</label><br>
					<textarea type="text" name="text" rows="5"	cols="40"> 
					</textarea>
				<br><br>
					<input type="submit" value="Send inn!">
			</form>
			</div>
		

			<div class="contact_information">
			<table class="contact_information_table">
				<tr>
					<th>Adresse: </th>
					<td>Daniel Hansens Gate 8</td>
				</tr>
				<tr>
					<th>Telefon: </th>
					<td>99999999</td>
				</tr>
				<tr>
					<th>Email: </th>
					<td>support@rrit.no</td>
				</tr>
			</table>
			</div>
		</div>








		</div>
	</body>
</html>